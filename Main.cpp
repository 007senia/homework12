#include <iostream>
#include <iomanip>
#include <string>

int N = 10;

void Numbers(int N, int Parameter)
{

	for (Parameter; Parameter <= N; Parameter += 2)
	{
		std::cout << Parameter << "\n";
	}
}

int main()
{
	for (int i = 0; i <= N; i+=2)
	{
		std::cout << i << "\n";
	}

	std::cout << "Type the value of N ";
	int N;
	std::cin >> N;
	std::cout << "Type 1 for odd or 0 for even numbers: ";
	int Argument;
	std::cin >> Argument;

	if (Argument != 1 && Argument != 0)
	{
		std::cout << "You failed! Try again!" << "\n";

	}
	else
	{
		Numbers(N, Argument);
	}
}